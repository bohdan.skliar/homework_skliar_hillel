"""Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів,
які містять дві голосні літери підряд."""

input_string = input("Введіть рядок: ")
words = input_string.split() 
count = 0

for word in words:
    double_vowels = False
    prev_vowel = None

    for letter in word:
        if letter.lower() in 'aeiou':
            if prev_vowel is None:
                prev_vowel = letter.lower()
            else:
                if letter.lower() == prev_vowel:
                    double_vowels = True
                    break
                else:
                    prev_vowel = letter.lower()

    if double_vowels:
        count += 1

print("Кількість слів з двома голосними літерами підряд:", count)
