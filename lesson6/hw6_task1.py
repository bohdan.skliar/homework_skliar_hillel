"""Напишіть декоратор, який вимірює час виконання функції"""
import time


def time_decorator(func):
    def execution_time(*args, **kwargs):
        start_time = time.time()
        print(f"Початок виконання функції {func.__name__}")
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Завершення виконання функції {func.__name__}")
        print(f"Час виконання: {end_time - start_time} секунд")
        return result
    return execution_time


@time_decorator
def example_function():
    time.sleep(0.01)


example_function()
