"""Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
 Якщо перетворити не вдається функція має повернути 0"""


def float_or_zero(value):
    try:
        float_value = float(value)
        return float_value
    except (ValueError, TypeError):
        return 0.0


print(float_or_zero("5430"))
print(float_or_zero("qwerty"))
print(float_or_zero(456))
