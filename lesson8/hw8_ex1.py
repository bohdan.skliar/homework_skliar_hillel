"""Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель",
наслідувані від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд. """


class Vehicle:
    def __init__(self, brand, model, year):
        self.brand = brand
        self.model = model
        self.year = year


class Car(Vehicle):
    def __init__(self, brand, model, year, body_type):
        super().__init__(brand, model, year)
        self.body_type = body_type


class Airplane(Vehicle):
    def __init__(self, brand, model, year, wingspan):
        super().__init__(brand, model, year)
        self.wingspan = wingspan


class Ship(Vehicle):
    def __init__(self, brand, model, year, draught):
        super().__init__(brand, model, year)
        self.draught = draught

