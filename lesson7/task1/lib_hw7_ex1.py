def get_adjective_of_age(age):
    if age == 1:
        return "рік"
    elif age in [2, 3, 4]:
        return "роки"
    else:
        return "років"

def main():
    age = int(input("Введіть ваш вік: "))
    adjective_of_age = get_adjective_of_age(age)

    if age < 7:
        print(f"Тобі ж {age} {adjective_of_age}! Де твої батьки?")
    elif age < 16:
        print(f"Тобі лише {age} {adjective_of_age}, а це фільм для дорослих!")
    elif age > 65:
        print(f"Вам {age} {adjective_of_age}? Покажіть пенсійне посвідчення!")
    elif str(7) in str(age):
        print(f"Вам {age} {adjective_of_age}, вам пощастить")
    else:
        print(f"Незважаючи на те, що вам {age} {adjective_of_age}, білетів всеодно нема!")
