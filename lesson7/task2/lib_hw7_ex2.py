# lib.py

def get_adjective_of_age(age):
    if age == 1:
        return "рік"
    elif age in [2, 3, 4]:
        return "роки"
    else:
        return "років"


import time


def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Функція {func.__name__} виконана за {execution_time:.4f} секунд")
        return result
    return wrapper
