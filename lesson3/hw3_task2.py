"""Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою).
Напишіть код, який визначить кількість слів, в цих даних"""

string = input("Write some words: ")
words = string.split()

word_count = len(words)

print(f"The number of words in the input string is: {word_count}")
