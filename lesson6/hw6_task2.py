"""Задекоруйте цим декоратором вашу програму "Касир"""""


def time_decorator(func):
    def execution_time(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Функція {func.__name__} виконана за {end_time - start_time:.6f} секунд")
        return result
    return execution_time


import time


@time_decorator
def get_adjective_of_age(age):
    if age == 1 or age in[21,31,41,51,61]:
        return "рік"
    elif age in [2, 3, 4]:
        return "роки"
    else:
        return "років"


def main():
    age = int(input("Введіть ваш вік: "))
    adjective_of_age = get_adjective_of_age(age)

    if age < 7:
        print(f"Тобі ж {age} {adjective_of_age}! Де твої батьки?")
    elif age < 16:
        print(f"Тобі лише {age} {adjective_of_age}, а це фільм для дорослих!")
    elif age > 65:
        print(f"Вам {age} {adjective_of_age}? Покажіть пенсійне посвідчення!")
    elif str(7) in str(age):
        print(f"Вам {age} {adjective_of_age}, вам пощастить")
    else:
        print(f"Незважаючи на те, що вам {age} {adjective_of_age}, білетів всеодно нема!")


if __name__ == "__main__":
    main()
