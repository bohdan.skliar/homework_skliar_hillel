"""Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'".
Слово та номер символу отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".
"""

text = input("Write some text: ")
char_numb = int(input("Write the character number: "))

if char_numb >= 1 and char_numb <= len(text):
    symbol = text[char_numb - 1]
    result = f"The {char_numb} symbol in '{text}' is '{symbol}'"
    print(result)
else:
    print("Invalid character number.")
