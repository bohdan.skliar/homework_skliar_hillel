"""Напишіть функцію, що приймає два аргументи. Функція повинна:
1. якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів
2. якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
3. у будь-якому іншому випадку повернути кортеж з цих аргументів"""


def two_arguments(arg1, arg2):
    if isinstance(arg1, (int, float)) and isinstance(arg2, (int, float)):
        return arg1 * arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return arg1 + arg2
    else:
        return arg1, arg2


result_1 = two_arguments(3.8, 88)
print(result_1)

result_2 = two_arguments("Hello, ", "world!")
print(result_2)

result_3 = two_arguments(123, "abc")
print(result_3) 
